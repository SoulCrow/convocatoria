/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nomina;

import DAO.EmpleadoDAOImpl;
import DAO.IEmpleadoDAO;
import POJO.Empleado;
import java.util.Scanner;

/**
 *
 * @author mr-code
 */
public class Nomina {

    /**
     * @param args the command line arguments
     */
    
    
    public static void main(String[] args) {
        boolean flag = true;
        int opcMenu;
        int idEmp;
        Scanner s = new Scanner(System.in);
        IEmpleadoDAO dao = new EmpleadoDAOImpl();
        
        
        while(flag){
            Menu();
            opcMenu = s.nextInt();
            switch(opcMenu){
                case 1:
                    dao.obtenerEmpleados().forEach(System.out::println);
                    break;
                case 2:
                    System.out.print("Ingrese código del empleado: ");
                    idEmp = s.nextInt();
                    Empleado empleado = dao.obtenerEmpleado(idEmp);
                    System.out.println(empleado);
                    break;
                case 3:
                    System.out.println("Ingrese el código del empleado:");
                    int id = s.nextInt();
                    s.nextLine();
                    System.out.println("Ingrese el nombre del empleado:");
                    String name  = s.nextLine();
                    System.out.println("Ingrese el apellido del empleado:");
                    String lastname  = s.nextLine();
                    System.out.println("Ingrese el cédula del empleado:");
                    String doc  = s.nextLine();
                    System.out.println("Ingrese el fecha de ingreso del empleado:");
                    String date  = s.nextLine();
                    System.out.println("Ingrese el salario del empleado:");
                    double salary  = s.nextDouble();
                    Empleado newEmpleado = new Empleado(id,doc,name,lastname,date,salary);
                    dao.agregarEmpleado(newEmpleado);
                    break;
                case 4:
                    System.out.println("Ingrese el código del empleado:");
                    int idUp = s.nextInt();
                    s.nextLine();
                    System.out.println("Ingrese el nombre del empleado:");
                    String nameUp  = s.nextLine();
                    System.out.println("Ingrese el apellido del empleado:");
                    String lastnameUp  = s.nextLine();
                    System.out.println("Ingrese el cédula del empleado:");
                    String docUp  = s.nextLine();
                    System.out.println("Ingrese el fecha de ingreso del empleado:");
                    String dateUp  = s.nextLine();
                    System.out.println("Ingrese el salario del empleado:");
                    double salaryUp  = s.nextDouble();
                    Empleado EmpleadoUp = new Empleado(idUp,docUp,nameUp,lastnameUp,dateUp,salaryUp);
                    dao.agregarEmpleado(EmpleadoUp);
                    break;
                case 5:
                    System.out.println("Ingrese el código del empleado:");
                    int idDel = s.nextInt();
                    dao.borrarEmpleado(dao.obtenerEmpleado(idDel));
                    break;
                case 6:
                    System.out.println("Ingrese el código del empleado:");
                    int idCol = s.nextInt();
                    dao.imprimirColilla(dao.obtenerEmpleado(idCol));
                    break;
                case 7:
                    flag = false;
                    break;
            }
        }
    }
    
    public static void Menu(){
        System.out.println("MENÚ");
        System.out.println("1 - Listar Empleados");
        System.out.println("2 - Listar Empleado");
        System.out.println("3 - Agregar Empleado");
        System.out.println("4 - Actualizar Empleado");
        System.out.println("5 - Borrar Empleado");
        System.out.println("6 - Imprimir colilla");
        System.out.println("7 - Salir");
    }
    
}
