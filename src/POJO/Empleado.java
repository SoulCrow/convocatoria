/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POJO;

/**
 *
 * @author mr-code
 */
public class Empleado {
    
    private int code;
    private String DocNo;
    private String names;
    private String lastNames;
    private String inDate;
    private double salary;

    public Empleado(int code, String DocNo, String names, String lastNames, String inDate, double salary) {
        this.code = code;
        this.DocNo = DocNo;
        this.names = names;
        this.lastNames = lastNames;
        this.inDate = inDate;
        this.salary = salary;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDocNo() {
        return DocNo;
    }

    public void setDocNo(String DocNo) {
        this.DocNo = DocNo;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getLastNames() {
        return lastNames;
    }

    public void setLastNames(String lastNames) {
        this.lastNames = lastNames;
    }

    public String getInDate() {
        return inDate;
    }

    public void setInDate(String inDate) {
        this.inDate = inDate;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return this.code + " - " + this.names + " " + this.lastNames;
    }
    
    
    
}
