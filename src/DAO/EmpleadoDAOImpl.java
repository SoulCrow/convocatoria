/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import POJO.Empleado;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mr-code
 */
public class EmpleadoDAOImpl implements IEmpleadoDAO {

    List<Empleado> empleados;

    public EmpleadoDAOImpl() {
        empleados = new ArrayList<>();
    }

    @Override
    public List<Empleado> obtenerEmpleados() {
        return empleados;
    }

    @Override
    public Empleado obtenerEmpleado(int id) {
        for (Empleado empleado : empleados) {
            if (empleado.getCode() == id) {
                return empleado;
            }
        }
        return null;
    }

    @Override
    public void agregarEmpleado(Empleado empleado) {
        empleados.add(empleado);
    }

    @Override
    public void actualizarEmpleado(Empleado empleado) {
        for (Empleado empleadof : empleados) {
            if (empleadof.getCode() == empleado.getCode()) {
                empleadof.setCode(empleado.getCode());
                empleadof.setDocNo(empleado.getDocNo());
                empleadof.setInDate(empleado.getInDate());
                empleadof.setLastNames(empleado.getLastNames());
                empleadof.setNames(empleado.getNames());
                empleadof.setSalary(empleado.getSalary());
            }
        }

    }

    @Override
    public void borrarEmpleado(Empleado empleado) {
        empleados.remove(empleado);
    }

    @Override
    public void imprimirColilla(Empleado empleado) {
        double INSS = empleado.getSalary() * 0.0625;
        double IR = ((empleado.getSalary() - INSS) - 8333.33) * 0.15;
        double SalarioNeto = empleado.getSalary() - INSS - IR;
        System.out.println("Nombre: " + empleado.getNames() + " " + empleado.getLastNames());
        System.out.println("INSS: " + INSS);
        System.out.println("IR: " + IR);
        System.out.println("Salario Neto: " + SalarioNeto);
    }

}
